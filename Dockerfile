ARG GOLANG_VERSION="latest"
FROM golang:$GOLANG_VERSION AS build

RUN go install github.com/boumenot/gocover-cobertura@v1.3 && \
    go install github.com/jstemmer/go-junit-report/v2@v2.1 && \
    go install github.com/google/wire/cmd/wire@v0.6 && \
    go install github.com/vektra/mockery/v2@v2.46 && \
    go install github.com/a-h/templ/cmd/templ@v0.2 && \
    go install honnef.co/go/tools/cmd/staticcheck@v0.5 && \
    go install gotest.tools/gotestsum@v1.12

# ---

ARG GOLANG_VERSION="latest"
FROM golang:$GOLANG_VERSION

# install alpine tooling
RUN { command -v apk >/dev/null 2>&1; apk add --no-cache make build-base git; } || true

COPY --from=build $GOPATH/bin/gocover-cobertura /usr/bin/gocover-cobertura
COPY --from=build $GOPATH/bin/go-junit-report /usr/bin/go-junit-report
COPY --from=build $GOPATH/bin/wire /usr/bin/wire
COPY --from=build $GOPATH/bin/mockery /usr/bin/mockery
COPY --from=build $GOPATH/bin/staticcheck /usr/bin/staticcheck
COPY --from=build $GOPATH/bin/templ /usr/bin/templ
COPY --from=build $GOPATH/bin/gotestsum /usr/bin/gotestsum
