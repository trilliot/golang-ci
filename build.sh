#!/bin/sh

set +e

for row in $(jq -r '.[] | @base64' go-versions.json); do
    echo $row
    _jq() {
        echo ${row} | base64 -d | jq -r ${1}
    }

    GOLANG_VERSION="$(_jq .current)"
    echo "Building version $GOLANG_VERSION" 
    docker build --build-arg GOLANG_VERSION=$GOLANG_VERSION -t $CI_REGISTRY_IMAGE:$GOLANG_VERSION .

    for GOLANG_VERSION_TAG in $(_jq '.tags[]'); do
        echo "Tagging $GOLANG_VERSION_TAG on $GOLANG_VERSION"
        docker tag $CI_REGISTRY_IMAGE:$GOLANG_VERSION $CI_REGISTRY_IMAGE:$GOLANG_VERSION_TAG
    done
done
