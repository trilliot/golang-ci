# Go - Docker image for CI

This project builds specific Docker images of the Go programming language that includes tools for CI.

Included tools are:

- [staticcheck](honnef.co/go/tools) (v0.5): linter
- [gocover-cobertura](https://github.com/boumenot/gocover-cobertura) (v1.3): code coverage reporter
- [gotestsum](https://github.com/gotestyourself/gotestsum) (v1.12): test runner with JUnit report output
- ~~[go-junit-report](https://github.com/jstemmer/go-junit-report) (v2.1): JUnit report converter~~
- [wire](https://github.com/google/wire) (v0.6): dependency injection
- [mockery](https://github.com/vektra/mockery) (v2.46.3): mocks generator
- [templ](https://github.com/a-h/templ) (v0.2): html templates generator


## Available tags

- [`1.23.3`](registry.gitlab.com/trilliot/golang-ci:1.23.3) (aliases: [`1.23`](registry.gitlab.com/trilliot/golang-ci:1.23), [`1`](registry.gitlab.com/trilliot/golang-ci:1), [`latest`](registry.gitlab.com/trilliot/golang-ci:latest))
- [`1.23.3-alpine`](registry.gitlab.com/trilliot/golang-ci:1.23.3-alpine) (aliases: [`1.23-alpine`](registry.gitlab.com/trilliot/golang-ci:1.23-alpine), [`1-alpine`](registry.gitlab.com/trilliot/golang-ci:1-alpine), [`alpine`](registry.gitlab.com/trilliot/golang-ci:alpine))

## How to build a new Go version?

Versions are build automatically following the listing in `go-versions.json`.

You simply need to change the contents of this file and push to trigger a new CI build.

**If this is a minor version:**

Change the corresponding `current` version in the file.

**If this is a major version:**

Add a new version in the file, e.g.:

```json
{"current": "X.Y.Z", "tags": ["X.Y", "X"]}
```

Do not forget to remove too specific tags on earlier versions.
